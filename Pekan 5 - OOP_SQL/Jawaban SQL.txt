SQL
Soal 1 Membuat Database
CREATE DATABASE library;

Soal 2 Membuat Table di Dalam Database
1. CREATE TABLE users ( 
    id int(11) AUTO_INCREMENT PRIMARY KEY, 
    name varchar(255), 
    email varchar(255), 
    password varchar(255) 
);


2. CREATE TABLE categories ( 
    id int(11) AUTO_INCREMENT PRIMARY KEY, 
    name varchar(255)
);

3. CREATE TABLE books ( 
    id int(11) AUTO_INCREMENT PRIMARY KEY, 
    title varchar(255),
    summary text,
    stock int(11),
    category_id int(11) not null,
    FOREIGN KEY (category_id) REFERENCES categories(id)
);

Soal 3 Memasukkan Data pada Table
1. INSERT INTO users (name, email, password)
VALUES ("John Doe", "john@doe.com", "john123");
INSERT INTO users (name, email, password)
VALUES ("Jane Doe", "jane@doe.com", "jenita123");

2. INSERT INTO categories (name) VALUES ("Novel");
INSERT INTO categories (name) VALUES ("Manga");
INSERT INTO categories (name) VALUES ("Comic");
INSERT INTO categories (name) VALUES ("History");
INSERT INTO categories (name) VALUES ("IT");

3. INSERT INTO books (title, summary, stock, category_id)
VALUES ("One piece", "The series focuses on Monkey D Luffy, a young man made of rubber", 50,2);
INSERT INTO books (title, summary, stock, category_id)
VALUES ("Laskar Pelangi", "Belitung is known for its richness in tin, making it one of Indonesia's richest islands", 25,1);
INSERT INTO books (title, summary, stock, category_id)
VALUES ("Your Name	", "Mitsuha Miyamizu, a high school girl living in the fictional town of Itomori in Gifu Prefecture's", 15,2);


Soal 4 Mengambil Data dari Database
1. Mengambil data users
SELECT id, name, email
FROM users;

2. Mengambil data Books
SELECT * FROM books
WHERE stock > 20;
SELECT * FROM books
WHERE title LIKE '%one%';

3. Menampilkan data items books dengan categories
SELECT books.*,categories.name as categories
FROM categories
INNER JOIN books
ON books.category_id = categories.id;


Soal 5 Mengubah Data dari Database
UPDATE books 
SET stock = 30
WHERE title="One Piece";
