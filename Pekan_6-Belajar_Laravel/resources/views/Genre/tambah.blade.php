@extends('layout.master')

@section('title')
    Halaman Tambah Genre
@endsection
    
@section('content')
<form action="/genre" method="post">
    @csrf
    <div class="form-group">
      <label>Nama</label>
      <input type="text" name="nama" class="form-control">
          
    </div>
    @error('nama')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
      <label for="exampleInputPassword1">Detail</label>
      <textarea name="detail" class="form-control" cols="30" rows="10"></textarea>
    </div>
    @error('detail')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <button type="submit" class="btn btn-primary">Submit</button>
</form>


@endsection