@extends('layout.master')

@section('title')
    Halaman Detail Data Genre
@endsection
    
@section('content')

<a href="/genre/create" class="btn btn-primary my-3">Create Data Genre</a>
<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">First</th>
        <th scope="col">Last</th>
        <th scope="col">Handle</th>
      </tr>
    </thead>
    <tbody>

      @forelse ($genres as  $key => $item)
        <tr>
          <th scope="row">{{$key+1}}</th>
          <td>{{$item -> nama}}</td>
          {{-- <td>{{$item -> detail}}</td> --}}
          <td>
            <form action="/genre/{{$item->id}}" method="post">
              <a href="/genre/{{$item->id}}" class="btn btn-sm btn-info my-3">Detail</a>
              {{-- <a href="/cast/{{$item->id}}" class="btn btn-sm btn-info my-3">Detail</a> --}}
              <a href="/genre/{{$item->id}}/edit" class="btn btn-sm btn-warning my-3">Edit</a>
              @csrf
              @method('delete')
              <input type="submit" onclick="return confirm('Are you sure?')" value="hapus" class="btn btn-sm btn-danger my-3">
            </form>
          </td>
        </tr>
      @empty
          <h1>Tidak ada data</h1>
      @endforelse

    </tbody>
  </table>


@endsection