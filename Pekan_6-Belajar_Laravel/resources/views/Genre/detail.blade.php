@extends('layout.master')

@section('title')
    Halaman Detail Genre
@endsection
    
@section('content')

    <h3>{{$genre -> nama}}</h3>
    <p><b>Deskripsi genre : </b> <br> {{$genre -> detail}}</p>

<div class="row">
    
    @forelse ($genre->film as $item)
        
            <div class="col-4">
                <div class="card">
                    <a href="/Film/{{$item->id}}">
                        <img src="{{asset('image/'.$item->poster)}}" class="card-img-top" alt="">
                    </a>
                    
                    <div class="card-body">
                        <h3>{{$item->judul}}</h3>
                        <p class="card-text">{{ Str::limit($item->ringkasan, 50) }}</p>
                    </div>
                </div>
            </div>
            
    @empty
        <h3>Tidak ada film di genre {{$genre -> nama}}</h3>
    @endforelse     
    
</div>

@endsection 