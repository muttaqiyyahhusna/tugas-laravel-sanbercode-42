@extends('layout.master')

@section('title')
    Halaman Detail Data Film
@endsection
    
@section('content')

@auth
    <a href="/Film/create" class="btn btn-primary my-3">Create Data Film</a>
@endauth


<div class="row">
    @forelse ($film as $item)
    
    <div class="col-4">
        <div class="card">
            <a href="/Film/{{$item->id}}">
                <img src="{{asset('image/'.$item->poster)}}" class="card-img-top" alt="">
            </a>
            
            <div class="card-body">
                <h3>{{$item->judul}}</h3>

                <span class="badge badge-pill badge-primary">{{$item->genre->nama}}</span>

                <p class="card-text">{{ Str::limit($item->ringkasan, 50) }}</p>
                <a href="/Film/{{$item->id}}" class="btn btn-secondary btn-block btn-sm">Read More</a>
                
                @auth
                <form action="Film/{{$item->id}}" method="post">
                @csrf
                @method('delete')
                    <div class="row my-2">
                        <div class="col">
                            <a href="/Film/{{$item->id}}/edit" class="btn btn-info btn-block btn-sm">Edit Film</a>
                        </div>
                        <div class="col">
                            <input type="submit" class="btn btn-danger btn-block btn-sm" value="Hapus Film">
                        </div>
                    </div>
                </form>
                @endauth    

            </div>
        </div>
    </div>
        
    @empty
        Tidak ada data film
    @endforelse

    
</div>




@endsection