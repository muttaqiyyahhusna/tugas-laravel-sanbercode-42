@extends('layout.master')

@section('title')
    Halaman Detail Film
@endsection
    
@section('content')

        <img src="{{asset('image/'.$film->poster)}}" class="card-img-top" alt="">
        
            <h3>{{$film->judul}}</h3>
            <p class="card-text">{{ $film->ringkasan}}</p>
            
            <hr>
            {{-- List komentar --}}
            <h4>List Komentar</h4>

            @forelse ($film->komentar as $item)
                <div class="card">
                    <div class="card-header">
                    {{$item->user->name}}
                    </div>
                    <div class="card-body">
                    <p class="card-text">{{$item->content}}</p>
                    {{-- <a href="#" class="btn btn-primary">Go somewhere</a> --}}
                    </div>
                </div>
            @empty
                Tidak ada komentar
            @endforelse
            
            <hr>
            {{-- tambah komentar --}}
            <form action="/komentar/{{$film->id}}" class="my-5" method="post">
                @csrf
                <textarea name="content" id="" class="form-control my-2" cols="30" rows="10" placeholder="Isi komentar"></textarea>
                @error('content')
                    <div class="alert alert-danger" role="alert">
                        {{$message}}
                    </div>
                @enderror

                <input type="submit" class="btn btn-primary btn-sm" value="Kirim" >
            </form>
            <br>
            <a href="/Film" class="btn btn-secondary btn-sm">Back</a>



@endsection