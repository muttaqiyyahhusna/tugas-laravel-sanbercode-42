@extends('layout.master')

@section('title')
    Halaman Detail Data Cast
@endsection
    
@section('content')

<a href="/cast/create" class="btn btn-primary my-3">Create Data</a>
<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">First</th>
        <th scope="col">Last</th>
        <th scope="col">Handle</th>
      </tr>
    </thead>
    <tbody>

      @forelse ($casts as  $key => $item)
        <tr>
          <th scope="row">{{$key+1}}</th>
          <td>{{$item -> nama}}</td>
          
          <td>
            <form action="/cast/{{$item->id}}" method="post">
              <a href="/cast/{{$item->id}}" class="btn btn-sm btn-info my-3">Detail</a>
              <a href="/cast/{{$item->id}}/edit" class="btn btn-sm btn-warning my-3">Edit</a>
              @csrf
              @method('delete')
              <input type="submit" onclick="return confirm('Are you sure?')" value="hapus" class="btn btn-sm btn-danger my-3">
            </form>
          </td>
        </tr>
      @empty
          <h1>Tidak ada data</h1>
      @endforelse

    </tbody>
  </table>


@endsection