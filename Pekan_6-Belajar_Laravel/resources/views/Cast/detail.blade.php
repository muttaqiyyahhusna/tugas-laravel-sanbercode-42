@extends('layout.master')

@section('title')
    Halaman Detail cast
@endsection
    
@section('content')

    <h1>Nama : {{$cast -> nama}}</h1>
    <h2>Umur : {{$cast -> umur}}</h2>
    <p>Bio   : {{$cast -> bio}}</p>

@endsection 