@extends('layout.master')

@section('title')
    Form Sign Up
@endsection
    
@section('content')

    <h1>Buat Account Baru! </h1>
    <h3>Sign Up Form</h3>

    <form action="/kirim" method="post">
        @csrf
    <label>First Name : </label><br><br>
    <input type="text" name="nama1"><br><br>
    <label>Last Name :</label><br><br>
    <input type="text" name="nama2"><br><br>
    <label>Gender</label><br><br>
    <input type="radio">Male<br><br>
    <input type="radio">Female<br><br>
    <input type="radio">Other<br><br>


    <label>Nationality : </label><br><br>
        <select name="nationality">
            <option value="Indonesian">Indonesian</option>
            <option value="Singapurian">Singapurian</option>
            <option value="Malaysian">Malaysian</option>
            <option value="Australian">Australian</option>
        </select>
    <br><br>

    <label>Language Spoken :</label><br><br>
    <input type="checkbox">Bahasa Indonesia<br><br>
    <input type="checkbox">English<br><br>
    <input type="checkbox">Other<br><br>

    <label>Bio : </label><br><br>
    <textarea name="bio" rows="10" cols="30"></textarea><br><br>

    <input type="submit" value="Sign Up">
    </form>

@endsection
    
