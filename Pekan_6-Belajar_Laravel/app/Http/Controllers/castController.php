<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\cast;

class castController extends Controller
{
    //fungsi membuat datanya
    public function create(){
        return view('Cast.tambah');
    }

    //fungsi menyimpan datanya
    public function simpan(Request $request){
        $validated = $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio'=>'required|min:10',
        ]);
    

    Cast::create([
        'nama' => $request['nama'],
        'umur' => $request['umur'],
        'bio' => $request['bio'],
    ]);

    return redirect('/cast');

    }

    public function index(){
        $casts=cast::all();
        return view('Cast.tampil', ['casts'=>$casts]);
    }

    public function show($id){
        $cast= cast::where('id',$id)->first();

        return view('Cast.detail', ['cast'=>$cast]);
    }

    public function edit($id){
        $cast= cast::where('id',$id)->first();

        return view('Cast.edit', ['cast'=>$cast]);
    }

    public function update($id, Request $request){
        $validated = $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio'=>'required|min:10',
        ]);

        $cast = cast::find($id);
 
        $cast->nama =  $request['nama'];
        $cast->umur =  $request['umur'];
        $cast->bio =  $request['bio'];
        
        $cast->save();

        return redirect('/cast');
       
    }

    public function destroy($id){
        $cast = cast::find($id);
 
        $cast->delete();
        return redirect('/cast');

    }

}
