<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('page.register');
    }

    public function kirim(Request $request)
    {
        $firstname = $request['nama1'];
        $lastname = $request['nama2'];
        return view('page.home', ['namadepan'=> $firstname,'namabelakang'=>$lastname]);

    }
}
