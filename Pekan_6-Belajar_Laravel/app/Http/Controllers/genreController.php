<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\genre;

class genreController extends Controller
{
    //fungsi membuat datanya
    public function create(){
        return view('Genre.tambah');
    }

    //fungsi menyimpan datanya
    public function simpan(Request $request){
        $validated = $request->validate([
            'nama' => 'required',
            'detail' => 'required|min:10',
        ]);
    

    genre::create([
        'nama' => $request['nama'],
        'detail' => $request['detail'],
    ]);

    return redirect('/genre');

    }

    public function index(){
        $genres=genre::all();
        return view('Genre.tampil', ['genres'=>$genres]);
    }

    public function show($id){
        $genre= genre::where('id',$id)->first();

        return view('Genre.detail', ['genre'=>$genre]);
    }

    public function edit($id){
        $genre= genre::where('id',$id)->first();

        return view('Genre.edit', ['genre'=>$genre]);
    }

    public function update($id, Request $request){
        $validated = $request->validate([
            'nama' => 'required',
            'detail' => 'required|min:10',
        ]);

        $genre = genre::find($id);
 
        $genre->nama =  $request['nama'];
        $genre->detail =  $request['detail'];
        
        
        $genre->save();

        return redirect('/genre');
       
    }

    public function destroygenre($id){
        $genre = genre::find($id);
 
        $genre->delete();
        return redirect('/genre');

    }

}
