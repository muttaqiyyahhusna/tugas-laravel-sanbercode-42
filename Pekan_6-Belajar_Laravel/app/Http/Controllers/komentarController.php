<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Komentar;
use Illuminate\Support\Facades\Auth;


class komentarController extends Controller
{
    //
    public function comment($id, Request $request){
        $validated = $request->validate([
            'content' => 'required|min:10',
            
        ]);

        $iduser=Auth::id();

        $komentar = new Komentar;

        $komentar->content=$request['content'];
        $komentar->film_id=$id;
        $komentar->user_id=$iduser;
        $komentar->point=5;

        $komentar->save();

        return redirect('/Film/'.$id);


    }
}
