<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\genre;
use App\Models\Film;
use File;


class filmController extends Controller
{
    public function __construct()
    {
        
        $this->middleware('auth')->except(['index','show']);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        //return "berhasil";
        $film=Film::get();
        return view('Film.tampil',['film'=>$film]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $genre = genre::get();
        return view('Film.tambah', ['genre'=>$genre]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validated = $request->validate([
            'judul' => 'required',
            'ringkasan' => 'required',
            'tahun' => 'required',
            'poster' => 'required|image|mimes:jpg,jpeg,png',
            'genre_id' => 'required',
            
        ]);

        $filename = time().'.'.$request->poster->extension();
        $request->poster->move(public_path('image'),$filename);
        
        $film = new Film;
        $film->judul =  $request->judul;
        $film->ringkasan =  $request->ringkasan;
        $film->tahun =  $request->tahun;
        $film->genre_id =  $request->genre_id;
        $film->poster=$filename;

        $film->save();

        return redirect('/Film');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $film=Film::find($id);
        return view('Film.detail',['film'=>$film]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $film=Film::find($id);
        $genre = genre::get();
        return view('Film.edit',['film'=>$film, 'genre'=>$genre]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validated = $request->validate([
            'judul' => 'required',
            'ringkasan' => 'required',
            'tahun' => 'required',
            'poster' => 'image|mimes:jpg,jpeg,png',
            
        ]);

        $film=Film::find($id);

        if($request->has('poster')){
            $path='image/';

            File::delete($path. $film->poster);

            $filename = time().'.'.$request->poster->extension();
            $request->poster->move(public_path('image'),$filename);

            $film->poster=$filename;
            $film->save();
        }

 
        $film->judul =  $request->judul;
        $film->ringkasan =  $request->ringkasan;
        $film->tahun =  $request->tahun;
        $film->genre_id =  $request->genre_id;
        $film->poster=$filename;
        
        
        $film->save();

        return redirect('/Film');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $film = Film::find($id);

        $path='image/';
        File::delete($path.$film->poster);
 
        $film->delete();

        return redirect('/Film');
    }
}
