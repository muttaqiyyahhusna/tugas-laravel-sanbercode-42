<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\castController;
use App\Http\Controllers\genreController;
use App\Http\Controllers\filmController;
use App\Http\Controllers\komentarController;





/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/test', function () {
//     return view('welcome');
// });

Route:: get('/', [IndexController::class, 'index']);
Route:: get('/register', [AuthController::class, 'register']);
Route:: post('/kirim', [AuthController::class, 'kirim']);

Route::get('/data-tables', function (){
    return view('page.table');
});

//CRUD DATA CAST

Route::group(['middleware' => ['auth']], function () {
    //
    //menuju form tambah data
    //Create data 
    Route:: get('/cast/create', [castController::class,'create']);
    //untuk menyimpan datanya
    Route:: post('/cast',[castController::class,'simpan']);

    //Read data
    //untuk menampilkan semua data
    Route:: get('/cast', [castController::class,'index']);
    //route dinamis yang bisa di dapat dari detail berdasarkan id
    Route:: get('/cast/{id}', [castController::class,'show']);

    //update data
    //menuju ke form inputan tambah data berdasarkan id
    Route::get('/cast/{id}/edit',[castController::class,'edit']);
    //update data di database berdasarkan id
    Route::put('/cast/{id}',[castController::class,'update']);

    //delete data
    Route::delete('/cast/{id}',[castController::class,'destroy']);

    //CRUD DATA CAST
    //menuju form tambah data
    //Create data 
    Route:: get('/genre/create', [genreController::class,'create']);
    //untuk menyimpan datanya
    Route:: post('/genre',[genreController::class,'simpan']);

    //Read data
    //untuk menampilkan semua data
    Route:: get('/genre', [genreController::class,'index']);
    //route dinamis yang bisa di dapat dari detail berdasarkan id
    Route:: get('/genre/{id}', [genreController::class,'show']);

    //update data
    //menuju ke form inputan tambah data berdasarkan id
    Route::get('/genre/{id}/edit',[genreController::class,'edit']);
    //update data di database berdasarkan id
    Route::put('/genre/{id}',[genreController::class,'update']);

    //delete data
    Route::delete('/genre/{id}',[genreController::class,'destroygenre']);

    //CRUD Komentar
    Route::post('/komentar/{id}',[komentarController::class, 'comment']);
});




//crud 
Route::resource('Film',filmController::class);
Auth::routes();
Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
